# diff-slave-topics-bug

Not sure if it's a bug or feature, but slave topics don't appear to be working
if master topic it's slave to is removed and re-created, without explicitly removing
slave topic with it.

1. run diffusion locally:
`docker run -it --rm -p 8080:8080 pushtechnology/docker-diffusion:6.3.0`
2. run the demo test; either from your IDE or via maven:
`./mvnw test -Dtest=SlaveTopicIssueDemo`
3. note test passes
4. repeat step 2)
5. slave topic assertion fails

Note this is not an issue if slave topic gets explicitly removed with its parent,
as demonstrated in `cleanUp()`.

Note `successfully added slave [topic/v1/slave], result=EXISTS` can be seen in the
logs for 2nd+ test run, suggesting slave topic indeed still exists.
Looks like the slave topic loses track of its parent if parent gets deleted and then
re-created.
