package diff.bug.slavetopicquestion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SlaveTopicBugApplication {

    public static void main(final String[] args) {
        SpringApplication.run(SlaveTopicBugApplication.class, args);
    }
}
