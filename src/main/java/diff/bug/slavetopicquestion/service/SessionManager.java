package diff.bug.slavetopicquestion.service;

import com.pushtechnology.diffusion.client.Diffusion;
import com.pushtechnology.diffusion.client.features.TopicUpdate;
import com.pushtechnology.diffusion.client.features.Topics;
import com.pushtechnology.diffusion.client.features.control.topics.TopicControl;
import com.pushtechnology.diffusion.client.session.Feature;
import com.pushtechnology.diffusion.client.session.Session;
import com.pushtechnology.diffusion.client.session.Session.State;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
@Slf4j
public class SessionManager {

    TopicControl topicControl;
    TopicUpdate updateControl;
    Topics topics;

    private Session session;

    void connect() {
        try {
            final String url = "ws://localhost:8080";
            session = Diffusion.sessions()
                    .reconnectionTimeout(1000)
                    .principal("admin")
                    .password("password")
                    .open(url);

            topicControl = feature(TopicControl.class);
            updateControl = feature(TopicUpdate.class);
            topics = feature(Topics.class);

            log.info("Connected to Diffusion on {}", url);
        } catch (final Throwable error) {
            log.warn("Unable to connect: {}", error.getMessage());
            reconnect();
        }
    }

    private <T extends Feature> T feature(final Class<T> featureInterface) {
        if (this.session == null || this.session.getState() != State.CONNECTED_ACTIVE) {
            throw new RuntimeException("Session not connected. Cannot create feature.");
        }

        return this.session.feature(featureInterface);
    }

    private void reconnect() {
        final int millisTillReconnect = 1500;
        log.warn("Will attempt reconnect in {} secs...", (millisTillReconnect / 1000.0));
        try {
            TimeUnit.MILLISECONDS.sleep(millisTillReconnect);
        } catch (final InterruptedException e) {
        }

        connect();
    }

    boolean isConnected() {
        return session != null && session.getState() == State.CONNECTED_ACTIVE;
    }
}