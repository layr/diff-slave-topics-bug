package diff.bug.slavetopicquestion.service;

import com.pushtechnology.diffusion.client.features.Topics;
import com.pushtechnology.diffusion.client.features.UpdateStream;
import com.pushtechnology.diffusion.client.topics.details.TopicSpecification;
import com.pushtechnology.diffusion.client.topics.details.TopicType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Consumer;
import java.util.stream.Stream;

import static com.pushtechnology.diffusion.client.Diffusion.newTopicSpecification;
import static org.springframework.util.CollectionUtils.isEmpty;

@Service
@Slf4j
public class DiffusionService {

    @Autowired
    private SessionManager sessionManager;

    private final Map<String, UpdateStream<String>> updaters = new ConcurrentHashMap<>();

    @PostConstruct
    public void init() {
        sessionManager.connect();
    }

    public <T> boolean subscribe(String topicPath, Class<T> valueClass, Consumer<T> consumer) {
        log.debug("subscribeTopic [{}]", topicPath);
        checkIsConnected();

        sessionManager.topics.addStream(topicPath, valueClass, new Topics.ValueStream.Default<T>() {
            public void onValue(String topicPath, TopicSpecification specification, T oldValue, T newValue) {
                log.debug("onValue topicPath={}, old={}, new={}", topicPath, oldValue, newValue);
                consumer.accept(newValue);
            }
        });

        try {
            sessionManager.topics.subscribe(topicPath).get(5, TimeUnit.SECONDS);
            log.debug("subscribed to [{}]", topicPath);
            return true;
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            log.error("subscribing to [{}] failed: {}", topicPath, e);
        }

        return false;
    }

    public boolean update(final String data, final String topic, final String slaveTopic) {
        log.info("updateTopic [{}]", topic);
        checkIsConnected();

        try {
            String[] slaves = new String[]{slaveTopic};
            UpdateStream<String> updateStream = updaters.get(topic);
            if (updateStream == null) {
                updateStream = newUpdateStream(topic, slaves);
            }

            updateStream
                    .set(data)
                    .whenComplete((result, ex) -> {
                        if (ex != null) {
                            log.error("setTopic [{}] failed; ex={}, cause={}", topic, ex, ex.getCause());
                        } else {
                            log.info("setTopic [{}] succeeded: {}", topic, result);
                        }
                    });

            addSlavesIfRequired(topic, slaves);
            return true;
        } catch (Throwable t) {
            log.error("updateTopic [{}] failed: {}", topic, t);
            updaters.remove(topic);
            return false;
        }
    }

    private UpdateStream<String> newUpdateStream(final String topic, final String[] slaves) {
        UpdateStream<String> updateStream;
        updateStream = sessionManager.updateControl.createUpdateStream(
                topic, newTopicSpecification(TopicType.STRING), String.class);
        if (slaves != null) {
            Stream.of(slaves).forEach(slave -> addSlave(topic, slave));
        }

        updaters.put(topic, updateStream);
        return updateStream;
    }


    private void addSlavesIfRequired(String topic, String[] slaves) {
        for (String slave : slaves) {
            log.debug("check for slave topic [{}] existence", slave);
            sessionManager.topics.fetchRequest()
                    .fetch(slave)
                    .whenComplete((result, ex) -> {
                        if (result != null) {
                            if (isEmpty(result.results())) {
                                log.debug("slave [{}] not found, adding...", slave);
                                addSlave(topic, slave);
                            } else {
                                log.debug("slave [{}] exists...", slave);
                            }
                        }

                        if (ex != null) {
                            log.error("slave check error", ex);
                        }
                    });
        }
    }

    private void addSlave(final String topic, final String slave) {
        log.info("adding slave [{}]", slave);
        sessionManager.topicControl.addTopic(slave, sessionManager.topicControl.newSpecification(TopicType.SLAVE)
                .withProperty(TopicSpecification.SLAVE_MASTER_TOPIC, topic))
                .whenComplete((result, ex) -> {
                    if (ex != null) {
                        log.error("slave add error", ex);
                    } else {
                        log.info("successfully added slave [{}], result={}", slave, result);
                    }
                });
    }

    public boolean delete(final String topic) {
        checkIsConnected();

        try {
            sessionManager.topicControl.removeTopics(topic).whenComplete((result, ex) -> {
                if (ex != null) {
                    log.warn("{} deletion failed: {}", topic, ex);
                }
            });

            updaters.remove(topic);
            return true;
        } catch (IllegalArgumentException ex) {
            log.error("{} deletion failed: {}", topic, ex);
            return false;
        }
    }

    private void checkIsConnected() {
        if (!sessionManager.isConnected()) {
            throw new RuntimeException("Not connected to diffusion");
        }
    }

    /**
     * Note this guy only retrieves the master topics
     */
    public List<String> getTopicsManaged() {
        return new ArrayList<>(updaters.keySet());
    }
}
