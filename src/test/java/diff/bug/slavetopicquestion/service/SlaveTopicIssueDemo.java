package diff.bug.slavetopicquestion.service;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import lombok.extern.slf4j.Slf4j;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext
@Slf4j
public class SlaveTopicIssueDemo {

    private final String TOPIC = "topic/v1";
    private final String SLAVE = TOPIC + "/slave";

    @Autowired
    DiffusionService diffusionService;

    @After
    public void cleanUp() {
        diffusionService.getTopicsManaged().forEach(diffusionService::delete);
//        diffusionService.delete(SLAVE); // note everything's ok if also slave topic(s) are explicitly removed
    }

    /**
     * All subsequent invocations after the first (and successful)
     * execution of this test will fail on the last (slave topic) assertion.
     */
    @Test
    public void testUpdateNewTopicWithSlaves() throws Exception {

        // given
        BlockingQueue<String> masterValues = new ArrayBlockingQueue<>(1);
        BlockingQueue<String> slaveValues = new ArrayBlockingQueue<>(1);

        // when
        diffusionService.subscribe(TOPIC, String.class, masterValues::add);
        boolean result = diffusionService.update("value", TOPIC, SLAVE);

        // then
        assertThat(result).isTrue();
        String value = masterValues.poll(5, TimeUnit.SECONDS);
        assertThat(value).isNotNull().isEqualTo("value");

        // when
        diffusionService.subscribe(SLAVE, String.class, slaveValues::add);

        // then
        value = slaveValues.poll(5, TimeUnit.SECONDS);
        assertThat(value).isNotNull().isEqualTo("value");
    }
}
